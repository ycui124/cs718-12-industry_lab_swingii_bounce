package ictgradschool.industry.lab_swingii.bounce;

import javax.swing.*;
import java.awt.*;

public class GemShape extends Shape {
    public GemShape() {
        super();
    }

    public GemShape(int x, int y) {
        super(x, y);
    }

    public GemShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public GemShape(int x, int y, int deltaX, int deltaY, int width, int height, Timer timer) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {
        Polygon polygon = null;
        if (fWidth<=40){
            polygon=new Polygon(new int[]{fX+fWidth/2,fX+fWidth,fX+fWidth/2,fX},new int[]{fY,fY+fHeight/2,fY+fHeight,fY+fHeight/2},4);
        }else {
            polygon= new Polygon(new int[]{fX + 20, fX + fWidth - 20, fX + fWidth, fX + fWidth - 20, fX + 20, fX}, new int[]{fY, fY, fY + (fHeight / 2), fY + fHeight, fY + fHeight, fY + (fHeight / 2)}, 6);
        }
        painter.drawPolygon(polygon);
    }
}

