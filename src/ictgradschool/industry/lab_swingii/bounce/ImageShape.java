package ictgradschool.industry.lab_swingii.bounce;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageShape extends Shape {

    public ImageShape() {
    }

    public ImageShape(int x, int y) {
        super(x, y);
    }

    public ImageShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }

    @Override
    public void paint(Painter painter) {
        try {
            BufferedImage img=ImageIO.read(new File("img.jpg"));
            painter.drawImage(img,fX,fY);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
