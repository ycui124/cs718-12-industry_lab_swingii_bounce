package ictgradschool.industry.lab_swingii.bounce;

import javax.swing.*;
import java.awt.*;

public class DynamicRectangleShape extends Shape {
    private Color color;

    public DynamicRectangleShape() {
    }

    public DynamicRectangleShape(int x, int y) {
        super(x, y);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Timer timer, Color color) {
        super(x, y, deltaX, deltaY, width, height);
        this.color=color;
    }
    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height, Timer timer) {
        super(x, y, deltaX, deltaY, width, height);
        this.color=Color.BLACK;
    }

    @Override
    public void paint(Painter painter) {
        painter.setColor(this.color);
        if (isSolid){
            painter.fillRect(fX,fY,fWidth,fHeight);
        }else {
            painter.drawRect(fX, fY, fWidth, fHeight);
        }
    }
    boolean isSolid=false;
    @Override
    public void move(int width, int height) {
        int nextX = fX + fDeltaX;
        int nextY = fY + fDeltaY;

        if (nextX <= 0) {
            nextX = 0;
            fDeltaX = -fDeltaX;
            this.isSolid = true;
        } else if (nextX + fWidth >= width) {
            nextX = width - fWidth;
            fDeltaX = -fDeltaX;
            this.isSolid = true;
        }

        if (nextY <= 0) {
            nextY = 0;
            fDeltaY = -fDeltaY;
            isSolid=false;
        } else if (nextY + fHeight >= height) {
            nextY = height - fHeight;
            fDeltaY = -fDeltaY;
            isSolid=false;
        }

        fX = nextX;
        fY = nextY;
    }

}
